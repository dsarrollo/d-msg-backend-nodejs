const { Router } = require('express');
const { check } = require('express-validator');
const { register, login, renewToken } = require('../controllers/auth');
const { validateFields } = require('../middlewares/validate-fields');
const { validateJWT } = require('../middlewares/validate-jwt');

const router = Router();

router.post('/register', [
    check('alias', 'Alias error').not().isEmpty(),
    check('email', 'Email error').isEmail(),
    check('password', 'Password error').not().isEmpty().isLength({ min: 6 }),
    validateFields
], register);

router.post('/login', [
    check('email', 'Email error').isEmail(),
    check('password', 'Password error').not().isEmpty().isLength({ min: 6 }),
    validateFields
], login);

router.get('/renewtoken', validateJWT, renewToken);

module.exports = router;