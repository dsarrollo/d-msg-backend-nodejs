const mongoose = require('mongoose');

/* mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true, useUnifiedTopology: true });

const Cat = mongoose.model('Cat', { name: String });

const kitty = new Cat({ name: 'Zildjian' });
kitty.save().then(() => console.log('meow')); */

const dbConnection = async () => {
    try {
        await mongoose.connect('mongodb+srv://chat_app:pgChXW5kTVSipvZ8@cluster0.bzjgl.mongodb.net/chat', { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
    } catch (error) {
        console.log(error);
        throw new Error('Error al conectar, porfavor contacte a Administración');
    }
}

module.exports = {
    dbConnection
};