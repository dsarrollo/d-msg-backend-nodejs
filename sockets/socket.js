const { stringify } = require('uuid');
const { io } = require('../index');

io.on('connection', client => {
    console.log('Cliente se conecto');

    client.emit('getBands', JSON.stringify({ bands: bands.getBands() }));

    client.on('deleteBand', (data) => {
        bands.deleteBand(data.id);
        client.broadcast.emit('deleteBand', { id: data.id }); // emite a todos menos al que envio el mensaje
    });

    client.on('addBand', (data) => {
        const bandAdd = new Band(data.name);
        bands.addBand(bandAdd);
        io.emit('addBand', JSON.stringify(bandAdd)); // emite a todos menos al que envio el mensaje
    });

    client.on('voteBand', (data) => {
        io.emit('voteBand', { id: data.id }); // emite a todos menos al que envio el mensaje
    });

    client.on('disconnect', () => {
        console.log('Cliente se desconecto');
        //client.emit('logout', { admin: 'message new' });
    });
});