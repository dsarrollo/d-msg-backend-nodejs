const { response } = require('express');
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const { generateJWT } = require('../shared/jwt');

const register = async (req, res = response) => {
    const { email, password } = req.body;
    try {
        const dataUser = await User.findOne({ email });
        if (existEmail) {
            return res.status(400).json({
                ok: false,
                msg: 'Email ya existe',
            });
        }
        const user = new User(req.body);

        // encriptar contraseña
        const salt = bcrypt.getSaltSync();
        user.password = bcrypt.hashSync(password, salt);

        await user.save();

        // generar JWT
        const token = await generateJWT(user._id);

        res.json({
            ok: true,
            user,
            token,
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador',
        });
    }
}

const login = async (req, res = response) => {
    const { email, password } = req.body;
    try {
        const dataUser = await User.findOne({ email });
        if (!dataUser) {
            return res.status(404).json({
                ok: false,
                msg: 'Email no existe',
            });
        }

        // comparar contraseñas
        const comparePassword = bcrypt.compareSync(password, dataUser.password);
        if (!comparePassword) {
            return res.status(404).json({
                ok: false,
                msg: 'Contraseña incorrecta',
            });
        }

        // generar JWT
        const token = await generateJWT(dataUser._id);

        res.json({
            ok: true,
            user: dataUser,
            token,
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador',
        });
    }
}

const renewToken = async (req, res = response) => {
    // generar JWT
    const token = await generateJWT(req.id);
    const dataUser = await User.findById(req.id);
    if (!dataUser) {
        return res.status(404).json({
            ok: false,
            msg: 'Error en el id, no existe',
        });
    }
    res.json({
        ok: true,
        dataUser,
        token,
    });
}
module.exports = {
    register,
    login,
    renewToken,
};