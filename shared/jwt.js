const jwt = require('jsonwebtoken');

const generateJWT = (id) => {
    return new Promise(() => {
        const payLoad = { id };
        jwt.sign(payLoad, process.env.jwtKey, {
            expiresIn: '24h',
        }, (err, token) => {
            if (err) {
                reject('No se puedo genera rel token');
            } else {
                resolve(token);
            }
        });
    });
}

module.exports = {
    generateJWT
}