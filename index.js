const { Socket } = require('dgram');
const express = require('express');
const app = express();
//DB config
require('./dataBase/config').dbConnection();
require('dotenv').config();

// lectura y parseo del body
app.use(express.json());

const path = require('path');

const serve = require('http').createServer(app);
module.exports.io = require('socket.io')(serve);
require('./sockets/socket');

//path publico
const publicPtah = path.resolve(__dirname, 'public');
app.use(express.static(publicPtah));

// Rutas http
app.use('/api/authentication', require('./routes/auth'));


serve.listen(process.env.PORT, (err) => {
    if (err) throw new Error(err);
    console.log('Servidor corriendo en el puerto', process.env.PORT);
});