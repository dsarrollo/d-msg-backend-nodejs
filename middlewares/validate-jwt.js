const jwt = require('jsonwebtoken');

const validateJWT = (req, res, next) => {
    const token = req.header('x-token');
    if (!token) {
        return res.status(401).json({
            ok: false,
            msg: 'No existe una autorización'
        });
    }
    try {
        const { id } = jwt.verify(token, process.env.jwtKey);
        req.id = id;
        next();
    } catch (error) {
        return res.status(401).json({
            ok: false,
            msg: 'No esta autorizado'
        });
    }

}
module.exports = {
    validateJWT
}